cd /data/hbase/data/default/
du -sh *
cd /usr/local/software/hbase-1.2.6/bin
./hbase shell

major_compact的操作目的
合并文件
清除删除、过期、多余版本的数据
提高读写数据的效率

604800  7day
describe  'TraceV2'
disable 'TraceV2'
alter 'TraceV2' , {NAME=>'S',TTL=>'604800'}
enable 'TraceV2'
disable 'TraceV2'
major_compact  'TraceV2'

1209600  14day
describe  'ApplicationTraceIndex'
disable 'ApplicationTraceIndex'
alter 'ApplicationTraceIndex' , {NAME=>'I',TTL=>'1209600'} 
enable 'ApplicationTraceIndex'
disable 'ApplicationTraceIndex'
major_compact  'ApplicationTraceIndex
