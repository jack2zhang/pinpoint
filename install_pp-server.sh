#!/bin/bash
#install java7

#install hbase 1.2.6
mkdir /data/hbase -p
tar zxvf hbase-1.2.6-bin.tar.gz -C /usr/local/
ln -s /usr/local/hbase-1.2.6/ /usr/local/hbase
rm -f /usr/local/hbase/conf/hbase-site.xml
cp hbase-site.xml /usr/local/hbase/conf/hbase-site.xml

echo "export JAVA_HOME=/usr/local/jdk" >> /usr/local/hbase/conf/hbase-env.sh
#start hbase
/usr/local/hbase/bin/start-hbase.sh
#check hbase
sz=`jps|grep -c HMaster`
if [[ sz -eq 1 ]];then
echo "hbase start ok"
/usr/local/hbase/bin/hbase shell hbase-create.hbase
else
echo "Failed to start hbase!"
fi


