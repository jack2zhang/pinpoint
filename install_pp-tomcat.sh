#!/bin/bash
#install pinpoint-collector
tar zxf apache-tomcat-8.0.36.tar.gz -C /usr/local/
mv /usr/local/apache-tomcat-8.0.36/ /usr/local/pinpoint-collector
rm /usr/local/pinpoint-collector/webapps/* -rf
cp pinpoint-collector-1.6.2.war /usr/local/pinpoint-collector/webapps/ROOT.war
rm /usr/local/pinpoint-collector/conf/ -rf
cp -r conf-pinpoint-collector/ /usr/local/pinpoint-collector/conf
/usr/local/pinpoint-collector/bin/startup.sh

#install pinpoint-web
tar zxf apache-tomcat-8.0.36.tar.gz -C /usr/local/
mv /usr/local/apache-tomcat-8.0.36/ /usr/local/pinpoint-web
rm /usr/local/pinpoint-web/webapps/* -rf
cp pinpoint-web-1.6.2.war /usr/local/pinpoint-web/webapps/ROOT.war
rm /usr/local/pinpoint-web/conf/ -rf
cp -r conf-pinpoint-web/ /usr/local/pinpoint-web/conf
/usr/local/pinpoint-web/bin/startup.sh

