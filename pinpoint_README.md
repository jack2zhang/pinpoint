pinpoint
=
  version:1.6.2

## 介绍

pinpoint是开源在github上的一款APM监控工具，它是用Java编写的，用于大规模分布式系统监控。它对性能的影响最小（只增加约3％资源利用率），安装agent是无侵入式的，只需要在被测试的Tomcat中加上3句话，打下探针，就可以监控整套程序了。

pinpoint [https://github.com/naver/pinpoint](https://github.com/naver/pinpoint)

### 安装java7

1. 解压到/usr/local/并做好软连接

	tar zxvf jdk-7u80-linux-x64.tar.gz -C /usr/local  
	ln -s /usr/local/jdk1.7.0_75 /usr/loacl/jdk

2. 设置环境变量 /etc/profile
          
    \#set java environment  
	export JAVA_HOME=/usr/local/jdk  
	export JRE_HOME=$JAVA_HOME/jre  
	export CLASSPATH=.:$CLASSPATH:$JAVA_HOME/lib:$JRE_HOME/lib  
	export PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH  

### 安装HBASE

1. 解压        

		tar zxvf hbase-1.2.6-bin.tar.gz -C /usr/local/   
 	    ln -s /usr/local/hbase-1.2.6 /usr/local/hbase  
 	    mkdir /data/hbase -p

2. 修改hbase-env.sh的JAVA_HOME环境变量位置

		cd /data/service/hbase/conf/  
		vi hbase-env.sh
	

3. 修改Hbase的配置信息

		vi hbase-site.xml

4. 启动hbase


		cd /usr/local/hbase/bin  
		./start-hbase.sh  


    查看hbase是否启动成功，成功会有HMaster进程

5. 初始化Hbase的pinpoint库


		./hbase shell /home/pp_res/hbase-create.hbase   


### 安装pinpoint-collector


### 安装pinpoint-web


### 部署pp-agent采集监控数据
